use anyhow;
use calamine::{open_workbook, DataType, Error, Range, RangeDeserializerBuilder, Reader, Xlsx};
use chrono::{self, NaiveDate, NaiveDateTime};
use colour;
use either::Either;
use enum_dispatch::enum_dispatch;
use nom_derive::*;
use serde::de::{self, Visitor};
use serde::{Deserialize, Serialize};
use serde_repr::{Deserialize_repr, Serialize_repr};
use std::borrow::{Borrow, BorrowMut};
use std::collections::HashMap;
use std::env;
use std::fmt::{self, Debug};
use std::ops::Deref;
// use std::os::windows::prelude::OpenOptionsExt;
use std::path::Path;
use std::path::PathBuf;
use thiserror;
use unicode_prettytable::*;

use tokio;

use crate::util::{
    self,
    deserializers::{de_opt_date, de_opt_f64, de_opt_string},
};

#[derive(Serialize, Deserialize, Debug, Clone)]
struct RawExcelRow {
    #[serde(default, rename = "INVOICE #", deserialize_with = "de_opt_string")]
    invoice_num: Option<String>,
    #[serde(default, rename = "BILLING DATE", deserialize_with = "de_opt_date")]
    billing_date: Option<NaiveDate>,
    #[serde(default, rename = "CLIENT/PROJECT", deserialize_with = "de_opt_string")]
    client_project: Option<String>,
    #[serde(default, rename = "AMOUNT", deserialize_with = "de_opt_f64")]
    amount: Option<f64>,
    #[serde(default, rename = "DATE REC'D", deserialize_with = "de_opt_date")]
    date_received: Option<NaiveDate>,
    #[serde(default, rename = "ACCOUNTING", deserialize_with = "de_opt_string")]
    quarter: Option<String>,
    #[serde(default, rename = "DIRECT EXPENSES", deserialize_with = "de_opt_f64")]
    direct_expenses: Option<f64>,
    #[serde(default, rename = "EXPENSES", deserialize_with = "de_opt_string")]
    expenses: Option<String>,
    #[serde(default, rename = "NET", deserialize_with = "de_opt_string")]
    net: Option<String>,
    #[serde(default, rename = "TOTAL EXPENSES", deserialize_with = "de_opt_f64")]
    total_expenses: Option<f64>,
    #[serde(default, rename = "GROSS PROFIT", deserialize_with = "de_opt_f64")]
    gross_profit: Option<f64>,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct ParsedBillingRow {
    id: Option<usize>,
    invoice_num: Option<String>,
    billing_date: Option<NaiveDate>,
    client_project: Option<String>,
    amount: Option<f64>,
    date_received: Option<NaiveDate>,
    quarter: Option<String>,
    direct_expenses: Option<f64>,
    expenses: Option<String>,
    net: Option<String>,
    total_expenses: Option<f64>,
    gross_profit: Option<f64>,
}

impl ParsedBillingRow {
    pub fn set_id(&mut self, id: usize) {
        self.id = Some(id);
    }
}

impl From<crate::RawExcelRow> for ParsedBillingRow {
    fn from(row: crate::RawExcelRow) -> Self {
        Self {
            id: None,
            invoice_num: row.invoice_num,
            billing_date: row.billing_date,
            client_project: row.client_project,
            amount: row.amount,
            date_received: row.date_received,
            quarter: row.quarter,
            direct_expenses: row.direct_expenses,
            expenses: row.expenses,
            net: row.net,
            total_expenses: row.total_expenses,
            gross_profit: row.gross_profit,
        }
    }
}

trait ParsedRow {
    fn get_fields(&self) -> HashMap<String, String>;
}

impl ParsedRow for ParsedBillingRow {
    fn get_fields(&self) -> HashMap<String, String> {
        let mut map: HashMap<String, String> = HashMap::new();
        map.insert(
            "ID".into(),
            self.id.clone().map_or_else(|| "None".into(), |x| x.to_string()),
        );
        map.insert(
            "Invoice Number".into(),
            self.invoice_num.clone().unwrap_or("None".into()),
        );
        map.insert(
            "Billing Date".into(),
            self.billing_date
                .clone()
                .map_or_else(|| "None".into(), |x| x.to_string()),
        );
        map.insert(
            "Client Project".into(),
            self.client_project.clone().unwrap_or("None".into()),
        );
        map.insert(
            "Amount".into(),
            self.amount.clone().map_or_else(|| "None".into(), |x| x.to_string()),
        );
        map.insert(
            "Date Received".into(),
            self.date_received
                .clone()
                .map_or_else(|| "None".into(), |x| x.to_string()),
        );
        map.insert("Quarter".into(), self.quarter.clone().unwrap_or("None".into()));
        map.insert(
            "Direct Expenses".into(),
            self.direct_expenses
                .clone()
                .map_or_else(|| "None".into(), |x| x.to_string()),
        );
        map.insert("Expenses".into(), self.expenses.clone().unwrap_or("None".into()));
        map.insert("Net".into(), self.net.clone().unwrap_or("None".into()));
        map.insert(
            "Total Expenses".into(),
            self.total_expenses
                .clone()
                .map_or_else(|| "None".into(), |x| x.to_string()),
        );
        map.insert(
            "Gross Profit".into(),
            self.gross_profit
                .clone()
                .map_or_else(|| "None".into(), |x| x.to_string()),
        );
        map
    }
}

trait FromParsed {
    type Output;
    type Err;
    fn from_parsed<P: ParsedRow>(parsed_row: P) -> Result<Self::Output, Self::Err>;
}

#[derive(Serialize, Deserialize, Debug, Clone)]
struct Row {
    fields: HashMap<String, String>,
}

impl FromParsed for Row {
    type Output = Self;
    type Err = anyhow::Error;

    fn from_parsed<P: ParsedRow>(parsed_row: P) -> Result<Self::Output, Self::Err> {
        Ok(Self {
            fields: parsed_row.get_fields(),
        })
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
struct Sheet<R: FromParsed> {
    name: String,
    rows: Vec<R>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
struct Document<R: FromParsed> {
    name: String,
    author: String,
    date: NaiveDate,
    sheets: Vec<Sheet<R>>,
}

impl std::fmt::Display for Row {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for (k, v) in &self.fields {
            write!(f, "{} = {}", k, v)?
        }
        Ok(())
    }
}

trait PrintTable {
    fn print_as_table(&self) -> std::fmt::Result;
}

impl PrintTable for Row {
    fn print_as_table(&self) -> std::fmt::Result {
        let mut input = vec![];
        for (k, v) in &self.fields {
            input.push(vec![&k[..], &v[..]]);
        }

        let table1 = TableBuilder::default()
            .rows(&input)
            .build()
            .map_err(|_| std::fmt::Error)?;
        println!("{}\n", table1);
        Ok(())
    }
}

struct ParseResult {
    index: usize,
    columns: Vec<ParseResultType>,
}

impl ParseResult {
    fn new() -> Self {
        Self {
            index: 0,
            columns: Vec::new(),
        }
    }
}

enum ParseResultType {
    String(Option<String>),
    Float64(Option<f64>),
    Int(Option<i32>),
    Other,
}

pub async fn get_columns<P>(filepath: P, sheet: &str) -> Result<Vec<String>, Error>
where
    P: AsRef<Path>,
{
    if !filepath.as_ref().exists() {
        return Err(Error::Msg("Filepath is empty"));
    }
    if sheet.eq("") {
        return Err(Error::Msg("Must specify a sheet"));
    }

    let mut workbook: Xlsx<_> = open_workbook(filepath)?;

    let mut columns = Vec::new();

    if let Some(Ok(r)) = workbook.worksheet_range(sheet) {
        if let Some(x) = r.rows().next() {
            columns.push(x.to_vec());
        }
    } else {
        return Err(Error::Msg("Couldn't read worksheet"));
    }

    let mut names = Vec::with_capacity(columns.iter().map(|row| row.len()).sum());

    for c in columns.into_iter().flatten() {
        if let Some(name) = c.get_string() {
            names.push(name.trim().to_owned());
        } else {
            return Err(Error::Msg("Unable to get column name"));
        }
    }
    Ok(names)
}

fn get_rows<T: AsRef<Path>>(xls_path: T) -> Result<(), Box<dyn std::error::Error>> {
    let mut excel: Xlsx<_> = open_workbook(xls_path)?;

    let range = excel
        .worksheet_range("Billing")
        .ok_or(calamine::Error::Msg("Cannot find Billing sheet"))??;

    let mut iter = RangeDeserializerBuilder::new()
        .from_range::<_, RawExcelRow>(&range)?
        .enumerate();
    while let Some((i, Ok(row))) = iter.next() {
        println!("Number {}", i);
        println!("Invoice #: {:?}", row.invoice_num);
        println!("Billing Date: {:?}", row.billing_date);
        println!("Client/Project: {:?}", row.client_project);
        println!("Amount: {:?}", row.amount);
        println!("Date Received: {:?}", row.date_received);
        println!("Quarter: {:?}", row.quarter);
        println!("Direct Expenses: {:?}", row.direct_expenses);
        println!("Expenses: {:?}", row.expenses);
        println!("Net Expenses: {:?}", row.net);
        println!("Total Expenses: {:?}", row.total_expenses);
        println!("Gross Profit: {:?}", row.gross_profit);
    }
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_get_columns() {
        let path_string = format!("{}", env::current_dir().unwrap().display());
        let filename = "gl1.xlsx";

        let target = Path::new(&path_string).join(filename);
        let sheet = "Billing";
        assert!(tokio_test::block_on(get_columns(&target, sheet)).is_ok());

        let names = [
            "INVOICE #",
            "BILLING DATE",
            "CLIENT/PROJECT",
            "AMOUNT",
            "DATE REC'D",
            "ACCOUNTING",
            "DIRECT EXPENSES",
            "EXPENSES",
            "NET",
            "TOTAL EXPENSES",
            "GROSS PROFIT",
        ];
        assert_eq!(tokio_test::block_on(get_columns(&target, sheet)).unwrap(), names);
        assert!(tokio_test::block_on(get_columns(&target, "")).is_err());
        assert!(tokio_test::block_on(get_columns("", sheet)).is_err());
    }
}
