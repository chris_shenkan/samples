use chrono::NaiveDate;
use either::Either;

pub mod deserializers;

pub fn from_days_since_1900(days_since_1900: i64) -> NaiveDate {
    let d1900 = NaiveDate::from_ymd(1900, 1, 1);
    d1900 + chrono::Duration::days(days_since_1900)
}

pub mod strings {
    use super::*;

    pub fn sentence_case(s: &str) -> String {
        let mut output: Vec<String> = Vec::new();
        s.split(" ")
            .collect::<Vec<&str>>()
            .into_iter()
            .for_each(|s| {
                let mut c = s.chars();
                let capped = match c.next() {
                    None => String::new(),
                    Some(f) => f.to_uppercase().collect::<String>() + c.as_str(),
            };
            output.push(capped);
        });
        output.join(" ")
    }

    pub fn sentence_case_ascii(s: &mut str) {
        if s == "" {
            return;
        }
        s[..1].make_ascii_uppercase();
        s[1..].make_ascii_lowercase();
    }
}

#[cfg(test)]
mod tests {
    use std::str::FromStr;

    use chrono::NaiveDate;

    use crate::from_days_since_1900;

    use super::strings::*;

    #[test]
    fn test_setence_case() {
        assert_eq!(sentence_case("hi friends"), "Hi Friends".to_string());
        assert_eq!(sentence_case("hi Friends"), "Hi Friends".to_string());
    }

    #[test]
    fn test_from_days_since_1900() {
        assert_eq!(
            crate::util::from_days_since_1900(44426),
            NaiveDate::from_ymd(2021, 08, 20)
        );
    }
}
