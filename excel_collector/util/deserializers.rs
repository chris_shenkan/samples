use calamine::DataType;
use serde::{Deserialize, Serialize};
use chrono::NaiveDate;

pub (crate) fn de_opt_string<'de, D>(deserializer: D) -> Result<Option<String>, D::Error> 
where
    D: serde::Deserializer<'de>,
{
    let data_type = calamine::DataType::deserialize(deserializer);
    match data_type {
        Ok(calamine::DataType::Error(e)) => Ok(None),
        Ok(calamine::DataType::Float(f)) => Ok(Some(f.to_string())),
        Ok(calamine::DataType::Int(i)) => Ok(Some(i.to_string())),
        Ok(calamine::DataType::String(s)) => Ok(Some(s)),
        _ => Ok(None),  
    }
}

pub (crate) fn de_opt_f64<'de, D>(deserializer: D) -> Result<Option<f64>, D::Error>
where
    D: serde::Deserializer<'de>,
{
    let data_type = calamine::DataType::deserialize(deserializer);
    match data_type {
        Ok(calamine::DataType::Error(_)) => Ok(None),
        Ok(calamine::DataType::Float(f)) => Ok(Some(f)),
        Ok(calamine::DataType::Int(i)) => Ok(Some(i as f64)),
        Ok(calamine::DataType::Empty) => Ok(None),
        _ => Ok(None),
    }
}

pub (crate) fn de_opt_date<'de, D>(deserializer: D) -> Result<Option<NaiveDate>, D::Error>
where
    D: serde::Deserializer<'de>,
{
    let data_type = calamine::DataType::deserialize(deserializer);
    match data_type {
        Ok(calamine::DataType::Error(_)) => Ok(None),
        Ok(calamine::DataType::Float(_f)) => Ok(data_type.map_or(None, |x| x.as_date())),
        Ok(calamine::DataType::Int(_i)) => Ok(data_type.map_or(None, |x| x.as_date())),
        Ok(calamine::DataType::DateTime(_d)) => Ok(data_type.map_or(None, |x| x.as_date())),
        Ok(calamine::DataType::Empty) => Ok(None),
        _ => Ok(None),        
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_de_opt_string() {
        
    }
}