# PDFGrader
 
### Version 1.0

## Authors 
- Chris Shenkan <chris@shenkan.dev>

## Installation
Run `pdfgrader_setup.exe` and follow the installation dialog.
 
## Running
The installer will have added the installation directory to PATH so `pdfgrader.exe` can be run from any terminal despite the current working directory, without using an absolute path.
 
### Command-line Arguments
PDFGrader has some commandline flags, only filename is required.  If no test number is provided, the program will guess based on the input file's data.  The default outfile name is output.pdf.  The --password argument is only needed when the file is encrypted
```
pdfgrader.exe --testno {1, 2, 3} --filename <filename> [--password <password>] --outfile <outfile> [--nooutput] 
```
(Note: only --outfile or --nooutput can be specified at a time)
 
For example, to grade example.pdf against test 1 and output it with the default output filename:
```
pdfgrader.exe -t 1 -f example.pdf
```
Note: test resources omitted