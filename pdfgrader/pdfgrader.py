#!/usr/bin/env python3
"""PDF Grader v0.2"""

import argparse
import pprint
import os
import sys
import io
import re
import pathlib
from termcolor import colored
from collections import OrderedDict
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import letter
from pdfminer.pdfparser import PDFParser
from pdfminer.pdfdocument import PDFDocument
from pdfminer.pdftypes import resolve1
from pdfminer.pdfdocument import PDFPasswordIncorrect
import PyPDF2

from answers import CORRECT_ANSWERS, SAMPLE_ANSWERS


class GradedTest:
    """Object representing a graded test.

    Populated with all of the values representing a graded test when built with __init__.

    Attributes:
        *same as the ``Args`` on `__init__`.
    """

    def __init__(self, test_number, grade_map, filepath, num_correct, total_counted, use_testing_dataset=False):
        """The init method for `GradedTest`.

        Args:
            test_number (int): Number of test to grade against [1, 2, 3].
            grade_map (dict{str: (bool, str)}): A dict that holds question numbers as keys mapped to (`bool`, `str`) tuples
                representing whether the answer was correct and the given response.
            filepath (str): Path to the input PDF file.
            num_correct (int): Number of questions marked correct.
            total_counted (int): Total number of questions counted.
            use_testing_dataset (bool, optional): Whether or not to use the sample (test) dataset.

        Raises:
            ValueError: If `test_number` isn't between 1 and 3.

        Returns:
            An instance of `GradedTest`.

        """
        if test_number:
            try:
                test_number = int(test_number)
            except TypeError:
                raise ValueError("test_number must be between 1 and 3")
        else:
            raise ValueError("test_number must be between 1 and 3")

        if test_number not in [1, 2, 3]:
            raise ValueError("test_number must be between 1 and 3")
        else:
            self.test_number = test_number

        self.grade_map = grade_map
        self.filepath = filepath
        self.num_correct = num_correct
        self.total_counted = total_counted
        self.use_testing_dataset = use_testing_dataset

    @property
    def num_blank(self):
        """Property returning the number of blank answers in the user's response.

        Takes `len()` of a dict filtering out any values in `self.grade_map.items()` that are
        `None`, meaning they're blank.

        Returns:
            int: Number of blank answers.
        """
        blanks = {k for k, v in self.grade_map.items() if v[0] is None}
        return len(blanks)

    @property
    def num_questions(self):
        """Property returning the number of questions in the test according to the answer key.

        Takes `len()` of `self.correct_answers` to find the correct number of questions in the test.

        Returns:
            int: Number of correct answers.
        """
        return len(self.correct_answers)

    @property
    def correct_answers(self):
        """Property returning a dict of the correct answers for the test represented by `self.test_number`.

        Indexes `CORRECT_ANSWERS` list by `self.test_number` as indicated, subtracting 1 for 0-based indexes.

        Returns:
            dict{str: str}: Mapping of question number to correct answer.
        """
        if not self.use_testing_dataset:
            return CORRECT_ANSWERS[int(self.test_number) - 1]
        else:
            return SAMPLE_ANSWERS[int(self.test_number) - 1]

    @property
    def wrong_answers(self):
        """Property returning a dict of the answers marked wrong, mapping question number to correct answer.

        Retrieves items from `self.grade_map` if the boolean value of the tuple is `False`.

        Returns:
            list{str}: List of questions that were marked wrong.
        """
        return [int(k.replace("Question", "")) for k, v in self.grade_map.items() if v[1] is False]

    @property
    def grade(self):
        """Property returning a float representing the grade calculated by dividing `self.num_correct` by
            `self.total_counted`.

        Asserts `self.num_correct` <= `self.total_counted`.  Returns 0.0 if `self.total_counted` is 0 to prevent
        a divide by zero error from occurring when a blank test is received.

        Returns:
            float: the grade as a value between 0 and 1.0.
        """
        assert self.num_correct <= self.total_counted
        assert self.num_correct <= self.num_questions
        if self.total_counted == 0 or self.num_questions == 0:
            return 0.0
        return self.num_correct / self.num_questions

    def print_summary(self):
        """Prints a summary of the `GradedTest` object."""
        print("\n")
        text = colored("Test Number:", "green")
        print(text, end=" ")
        print(self.test_number)
        pp = pprint.PrettyPrinter(indent=4)
        text = colored("Graded Response Map:", "green")
        print(text, end="\n")
        pp.pprint(self.grade_map)
        print("\n")
        text = colored("Questions Wrong:", "red")
        print(text, end=" ")
        print(self.wrong_answers)
        print("\n")
        text = colored("Number Correct:", "green")
        print(text, end=" ")
        print(self.num_correct)
        text = colored("Number Incorrect:", "red")
        print(text, end=" ")
        print(len(self.wrong_answers))
        text = colored("Number Blank:", "red")
        print(text, end=" ")
        print(self.num_blank)
        text = colored("Total Graded:", "green")
        print(text, end=" ")
        print(self.total_counted)
        text = colored("Total Questions:", "green")
        print(text, end=" ")
        print(self.num_questions)
        text = colored("Grade:", "green")
        print(text, end=" ")
        print("{0} / {1} = {2:.0%}".format(self.num_correct, self.num_questions, self.grade))

    # never used as requirements changed
    def generate_output_pdf(self, outpath, password=None):
        """Generates a marked and graded output PDF file.

        Marks up a canvas with grades and incorrect questions and
        then merges it with a copy of the original PDF file.

        Args:
            outpath (str): The output filepath to use for the marked PDF.
            password (str, optional default=None): Password to unlock the file if it is encrypted.

        Raises:
            Exceptions from the file io and the `PyPDF2` module.

        """
        if not outpath:
            outfile = "output.pdf"
        elif os.path.isfile(outpath):
            outfile = outpath
        elif os.path.isdir(outpath):
            outfile = os.path.join(outpath, "output.pdf")
        else:
            outfile = "output.pdf"

        packet = io.BytesIO()
        can = canvas.Canvas(packet, pagesize=letter)
        # write to new canvas
        can.drawString(10, 100, "Hello world")
        can.save()

        packet.seek(0)

        new_pdf = PyPDF2.PdfFileReader(packet)
        existing_pdf = PyPDF2.PdfFileReader(open(self.filepath, "rb"))
        output = PyPDF2.PdfFileWriter()
        if existing_pdf.isEncrypted:
            if password:
                existing_pdf.decrypt(password)
            else:
                print("File is encrypted, please provide a password with the -p/--password switch")
                sys.exit()
        page = existing_pdf.getPage(0)
        page.mergePage(new_pdf.getPage(0))
        output.addPage(page)
        # finally, write "output" to a real file
        outputStream = open(outfile, "wb")
        output.write(outputStream)
        outputStream.close()


def check_answers(response, filepath, test_number, use_testing_dataset=False):
    """Checks answers and instantiates a `GradedTest` object with the results.

    Checks the answers provided in the response dict against the provided answer key for
    `test_number`.  Creates a new dict mapping question numbers to response strings and a boolean
    representing whether or not the answer what correct.

    Args:
        response (dict{str: str}): A dict mapping the question numbers to the test taker's responses.
        filepath (str): Path of the input PDF file.
        test_number (int): Number of test to grade against [1, 2, 3].
        use_testing_dataset (bool, optional): Whether or not to use the sample (test) dataset.

    Raises:
        ValueError: If `test_number` isn't between 1 and 3.

    Returns:
        `GradedTest`: The object representing the results of the checked test answers.
    """
    grade_map = OrderedDict()
    num_correct = 0
    total_counted = 0
    if not test_number and not use_testing_dataset:
        # short circuit `test_number` to 1 for now.
        # test_number = detect_test_number()
        # for now test_number defaults to 1, until detect_test_number() is implemented
        test_number = 1
    else:
        test_number = int(test_number)
        test_number = 1

    if test_number not in [1, 2, 3]:
        raise ValueError("test_number must be between 1 and 3")

    if not use_testing_dataset:
        correct = CORRECT_ANSWERS[test_number - 1]
    else:
        correct = SAMPLE_ANSWERS[test_number - 1]

    response = {k: v for k, v in response.items() if k.startswith("Question")}

    for key in response.keys():
        try:
            formatted_key = re.sub(r"(\d+(\.\d+)?)", r" \1", key)
            if response[key] in correct[key]:
                num_correct += 1
                grade_map[formatted_key] = (response[key], True)
            else:
                grade_map[formatted_key] = (response[key], False)
            total_counted += 1
        except KeyError as e:
            print(f"invalid respone or answer set info: {e}")
            sys.exit()
    graded = GradedTest(
        test_number, grade_map, filepath, num_correct, total_counted, use_testing_dataset=use_testing_dataset
    )
    return graded


def detect_test_number(filepath):
    """Detects the test number to compare the responses against.

    Reads the provided PDF to determine the test version from
    specific text in the top of the file.

    Args:
        filepath (str): Path of the input PDF file.
    """
    pass


def _get_response(filepath, password=None):
    """Returns a mapping of questions to responses by reading the submitted PDF file.

    Reads the provided PDF file and parses out any AcroForm fields.  Parses out of the
    keys 'T' and 'V' which represent the form fields and their values.

    Args:
        filepath (str): Path of the input PDF file.
        password (str, optional default=None): Password to unlock the file if it is encrypted.

    Returns:
        dict{str: str}: Mapping of question number to test taker's response.
    """
    with open(filepath, "rb") as pdf_file:
        parser = PDFParser(pdf_file)
        if password:
            doc = PDFDocument(parser, password=password)
        else:
            if check_encrypted(filepath):
                print("File is encrypted, please provide a password with the -p/--password switch")
                sys.exit()
            doc = PDFDocument(parser)

        fields = resolve1(doc.catalog["AcroForm"])["Fields"]
        response = OrderedDict()
        for i in fields:
            field = resolve1(i)
            name = str(field.get("T"), "utf-8")

            value = field.get("V")

            if value is not None:
                value = str(value)
                if value[0] == r"/":
                    value = value[2:-1]
                    value = str(value)
                    response[name] = value
            else:
                response[name] = None
    return response


def check_encrypted(filepath):
    with open(filepath, "rb") as file:
        existing_pdf = PyPDF2.PdfFileReader(file)
        if existing_pdf.isEncrypted:
            return True
    return False


def get_response(filepath, password=None):
    """Wrapper around `_get_response`.  Handles whether or not to pass a `password`.

    If a password is passed on the command-line, calls `_get_response` with a password.  Otherwise,
    `password` is set to `None` by default and `_get_response` is called without it.

    Arguments: Object representing the parsed arguments.
        filepath (str): Path of the input PDF file.
        password (str, optional default=None): Password to unlock the file if it is encrypted.

    Returns:
        dict{str: str}: Mapping of question number to test taker's response.
    """
    if password:
        response = _get_response(filepath, bytes(password, "utf-8"))
    else:
        response = _get_response(filepath)
    return response


def parse_args():
    """Set up the `ArgumentParser` and parse the command-line arguments.

    Returns:
        Arguments: Object representing the parsed arguments.
    """
    parser = argparse.ArgumentParser(
        prog="pdfgrader", description="Program to grade multiple choice tests in PDF form"
    )

    parser.add_argument("-f", "--filepath", help="the path to the PDF to be graded", required=True)
    parser.add_argument("-p", "--password", default=None, help="the password to the encrypted PDF", required=False)
    parser.add_argument(
        "-t", "--testno", choices=["1", "2", "3"], default=None, help="test number to grade", required=False
    )
    parser.add_argument("-o", "--outpath", default=None, help="output pdf filepath", required=False)
    parser.add_argument("-n", "--nooutput", help="whether to display the output PDF", action="store_true")
    args = parser.parse_args()
    if not os.path.isfile(args.filepath):
        print(f"PDF file does not exist at path: {args.filepath}\n")
        parser.print_help()
        sys.exit()
    if pathlib.Path(args.filepath).suffix != ".pdf":
        print("file must have extension .pdf\n")
        parser.print_help()
        sys.exit()
    if args.outpath:
        if args.nooutput:
            print("cannot use --nooutput argument with --outpath\n")
            parser.print_help()
            sys.exit()
        if not os.path.exists(args.outpath):
            print(f"specified output path does not exist: {args.outpath}\n")
            parser.print_help()
            sys.exit()
    return args


def main():
    args = parse_args()
    try:
        response = get_response(args.filepath, args.password)
    except PDFPasswordIncorrect:
        print("Provided password is incorrect")
        sys.exit()
    graded = check_answers(response, args.filepath, args.testno)
    graded.print_summary()
    if not args.nooutput:
        if args.password:
            graded.generate_output_pdf(args.outpath, args.password)
        else:
            graded.generate_output_pdf(args.outpath)


if __name__ == "__main__":
    main()
