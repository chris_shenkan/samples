# PDFGrader
 
### Version 1.0

## Authors 
- Chris Shenkan <chris@shenkan.dev>

## Running
Run the .exe or pdfgrader.py

### Install Dependencies
You need to install required dependency packages either globally or in a virtual environment

#### Globally
```
pip install -r requirements
```
 
#### With a virtual environment
```
python -m venv myvenv
./myvenv/Scripts/activate
pip install -r requirements
```
 
 ### Building an executable
You can generate an executable with pyinstaller and run that instead of invoking Python
 
Install the pyinstaller package with pip (can be installed system-wide) as well as the other dependencies listed in `requirements`
 
Run pyinstaller to generate the executable:
```
pyinstaller --onefile pdfgrader.py
```
or
```
python -m pyinstaller --onefile pdfgrader.py
```
Afterwards you can substitute `python pdfgrader.py` for `pdfgrader.exe` on Windows or `pdfgrader` on Linux which has been placed in the ./dist directory

### Creating the Installer (Windows)
The setup installer is created using InnoSetup.  The scripts to create the installer are located in `scripts/`.  Using InnoSetup, you can compile and run the `create_installer.iss` script which will create the `pdfgrader_setup.exe` file.  This is the file that should be signed and distributed to end users.
 
### Command-line Arguments
PDFGrader has some commandline flags, only filename is required.  If no test number is provided, the program will guess based on the input file's data.  The default outfile name is output.pdf.  The --password argument is only needed when the file is encrypted
```
pdfgrader.exe --testno {1, 2, 3} --filename <filename> [--password <password>] --outfile <outfile> [--nooutput] 
```
(Note: only --outfile or --nooutput can be specified at a time)
 
For example, to grade example.pdf against test 1 and output it with the default output filename:
```
pdfgrader.exe -t 1 -f example.pdf
```

### Testing

Tests are run using pytests

Note: test resources omitted

```
pytest tests.py
```