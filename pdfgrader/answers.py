# Answer key for Test 1.
TEST1_CORRECT_ANSWERS = {
    "Question1": ("C",),
    "Question2": ("C",),
    "Question3": ("A",),
    "Question4": ("A",),
    "Question5": ("D",),
    "Question6": ("C",),
    "Question7": ("C",),
    "Question8": ("C",),
    "Question9": ("B",),
    "Question10": ("B",),
    "Question11": ("A",),
    "Question12": ("B", "C"),
    "Question13": ("A",),
    "Question14": ("A",),
    "Question15": ("A",),
    "Question16": ("A",),
    "Question17": ("C",),
    "Question18": ("C",),
    "Question19": ("D",),
    "Question20": ("D",),
    "Question21": ("D",),
    "Question22": ("B",),
    "Question23": ("A",),
    "Question24": ("B",),
    "Question25": ("D",),
    "Question26": ("D",),
    "Question27": ("B",),
    "Question28": ("A",),
    "Question29": ("C",),
    "Question30": ("C",),
    "Question31": ("C",),
    "Question32": ("B",),
    "Question33": ("C",),
    "Question34": ("A",),
    "Question35": ("A",),
    "Question36": ("C",),
    "Question37": ("A",),
    "Question38": ("B",),
    "Question39": ("D",),
    "Question40": ("B",),
    "Question41": ("C",),
    "Question42": ("C",),
    "Question43": ("D",),
    "Question44": ("C",),
    "Question45": ("A",),
    "Question46": ("C",),
    "Question47": ("A",),
    "Question48": ("A",),
    "Question49": ("D",),
    "Question50": ("D",),
    "Question51": ("B",),
    "Question52": ("B",),
    "Question53": ("C",),
    "Question54": ("C",),
    "Question55": ("A",),
    "Question56": ("D",),
    "Question57": ("C",),
    "Question58": ("D",),
    "Question59": ("B",),
    "Question60": ("A",),
    "Question61": ("C",),
    "Question62": ("B",),
    "Question63": ("C",),
    "Question64": ("D", "B"),
    "Question65": ("B",),
    "Question66": ("D",),
    "Question67": ("D",),
}

# Answer key for Test 2.
TEST2_CORRECT_ANSWERS = {}

# Answer key for Test 3.
TEST3_CORRECT_ANSWERS = {}

# List of test answer keys.
CORRECT_ANSWERS = [TEST1_CORRECT_ANSWERS, TEST2_CORRECT_ANSWERS, TEST3_CORRECT_ANSWERS]

# Answer key for sample test 1.
SAMPLE1_CORRECT_ANSWERS = {"Question1": ("A",), "Question2": ("B",), "Question3": ("C",)}

# List of sample answer keys (for testing).
SAMPLE_ANSWERS = [SAMPLE1_CORRECT_ANSWERS]
