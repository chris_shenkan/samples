import pytest
from answers import SAMPLE_ANSWERS
from pdfgrader import check_answers, get_response
import pathlib


class TestGrader:
    def test_get_response(self):
        expected_response = {"Question1": "A", "Question2": "B", "Question3": "C"}
        filepath = pathlib.Path("samples/sample1.pdf")
        response = get_response(str(filepath))
        assert response == expected_response

    def test_get_response_with_blanks(self):
        expected_response = {"Question1": "A", "Question2": None, "Question3": "C"}
        filepath = pathlib.Path("samples/sample2.pdf")
        response = get_response(str(filepath))
        assert response == expected_response

    def test_check_answers(self):
        test_response = {"Question1": "A", "Question2": "B", "Question3": "C"}
        filepath = pathlib.Path(
            "samples/sample1.pdf"
        )  # filepath argument is not actually used inside the class in this test
        graded = check_answers(test_response, str(filepath), 1, use_testing_dataset=True)
        expected_grade_map = {"Question 1": ("A", True), "Question 2": ("B", True), "Question 3": ("C", True)}
        assert graded.num_correct == 3
        assert graded.total_counted == 3
        assert graded.grade_map == expected_grade_map
        assert pathlib.Path(graded.filepath).resolve() == filepath.resolve()

    def test_check_answers_with_blanks(self):
        test_response = {"Question1": "A", "Question2": None, "Question3": "C"}
        filepath = pathlib.Path(
            "samples/sample1.pdf"
        )  # filepath argument is not actually used inside the class in this test
        graded = check_answers(test_response, str(filepath), 1, use_testing_dataset=True)
        expected_grade_map = {"Question 1": ("A", True), "Question 2": (None, False), "Question 3": ("C", True)}
        assert graded.num_blank == 1
        assert graded.num_correct == 2
        assert graded.total_counted == 3
        assert graded.grade_map == expected_grade_map
        assert pathlib.Path(graded.filepath).resolve() == filepath.resolve()

    def test_correct_test1_pdf(self):
        filepath = pathlib.Path("test-pdfs/correct.pdf")
        response = get_response(filepath)
        graded = check_answers(response, str(filepath), 1)

        assert graded.grade == 1.0
        assert graded.num_correct == 67
        assert graded.num_blank == 0
        assert graded.total_counted == 67
        assert graded.num_questions == 67
        assert pathlib.Path(graded.filepath).resolve() == filepath.resolve()

    def test_blank_test1_pdf(self):
        filepath = pathlib.Path("test-pdfs/blank.pdf")
        response = get_response(filepath)
        graded = check_answers(response, str(filepath), 1)

        assert graded.grade == 0.0
        assert graded.num_correct == 0
        assert graded.num_blank == 67
        assert graded.total_counted == 67
        assert graded.num_questions == 67
        assert pathlib.Path(graded.filepath).resolve() == filepath.resolve()

    def test_detect_test_number(self):
        pass

    def test_generate_output_pdf(self):
        pass
