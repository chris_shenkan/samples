use anyhow;
use clap::{AppSettings, Clap};
use colour::{blue, blue_ln, cyan, cyan_ln, dark_yellow, dark_yellow_ln, green, green_ln, red, red_ln};
use crossbeam::{channel::unbounded, select};
use futures::{future, Future};
use notify::{watcher, DebouncedEvent, RecommendedWatcher, RecursiveMode, Watcher};
use rand::prelude::*;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::error::Error;
use std::path::{Path, PathBuf};
use std::pin::Pin;
use std::slice::SliceIndex;
use std::sync::{Arc, Mutex};
use thiserror::Error;
use tokio::io::{stdin, AsyncBufReadExt, AsyncRead, AsyncWriteExt, BufReader, BufStream, Stdin};
use tokio::signal;
#[cfg(unix)]
use tokio::signal::unix::{signal, Signal, SignalKind};
use tokio::sync::mpsc::unbounded_channel;
use tokio::task::JoinHandle;
use tokio::{
    self,
    sync::mpsc::{Receiver, Sender},
    time::Duration,
};

struct MyWatcher;

type Event = DebouncedEvent;
type EventStream = Receiver<Event>;

#[derive(Clap)]
struct Opts {
    #[clap(short, long)]
    verbose: bool,
}

#[derive(Error, Debug)]
enum MyError {
    #[error(transparent)]
    InputError(#[from] anyhow::Error),
    #[error("io error")]
    IoError(#[from] std::io::Error),
}

pub type BoxError = std::boxed::Box<dyn std::error::Error + Send + Sync + 'static>;

#[tokio::main]
async fn main() {
    let opts = Opts::parse();
    if opts.verbose {
        println!("Running with verbose flag");
    } else {
        println!("Running without verbose flag");
    }

    let (txw, mut rxw): (std::sync::mpsc::Sender<Event>, std::sync::mpsc::Receiver<Event>) = std::sync::mpsc::channel();
    let (tx_stdin, mut rx_stdin): (
        std::sync::mpsc::Sender<Vec<PathBuf>>,
        std::sync::mpsc::Receiver<Vec<PathBuf>>,
    ) = std::sync::mpsc::channel();
    let (shutdown_send, mut shutdown_recv): (
        tokio::sync::mpsc::UnboundedSender<()>,
        tokio::sync::mpsc::UnboundedReceiver<()>,
    ) = tokio::sync::mpsc::unbounded_channel();
    let (shutdown_notify_send, mut shutdown_notify_recv): (
        tokio::sync::broadcast::Sender<()>,
        tokio::sync::broadcast::Receiver<()>,
    ) = tokio::sync::broadcast::channel();
    let tx_stdin2 = tx_stdin.clone();
    let stdin_handle = tokio::spawn(async move {
        let x = get_input(">").await; //.map_err(|e| Box::new(e) as Box<dyn Error + Send + Sync + 'static>)?;
        let v = x
            .into_iter()
            .map(|x| {
                println!("Got {:?} from stdin", &x);
                x.into()
            })
            .collect::<Vec<PathBuf>>();
        tx_stdin2.send(v).unwrap();
        println!("done stdin");
        drop(tx_stdin2);
    });

    let txw2 = txw.clone();
    let watch_handle = tokio::spawn(async move {
        let mut watcher: RecommendedWatcher = Watcher::new(txw2, Duration::from_secs(1)).unwrap();
        while let Ok(v) = rx_stdin.recv() {
            for path in v {
                let path = format_path(path);
                println!("Got {:?} to watch", &path);
                // spawns std::thread
                watcher.watch(Path::new(&path), RecursiveMode::Recursive).unwrap();
            }
        }
    });

    tokio::time::sleep(Duration::from_secs(1)).await;

    let (txw_tokio, mut rxw_tokio) = crossbeam::channel::unbounded();
    let txw_tokio2 = txw_tokio.clone();
    let shutdown_notify_send_other = shutdown_notify_send.clone();
    let other_handle = tokio::spawn(async move {
        while let Ok(x) = rxw.recv() {
            if opts.verbose {
                match &x {
                    DebouncedEvent::Create(p) => {
                        green!("[Info] ");
                        cyan!("CREATE -> ");
                        println!("{:?}", p);
                    }
                    DebouncedEvent::Chmod(p) => {
                        green!("[Info] ");
                        cyan!("CHMOD -> ");
                        println!("{:?}", p);
                    }
                    DebouncedEvent::Write(p) => {
                        green!("[Info] ");
                        cyan!("WRITE -> ");
                        println!("{:?}", p);
                    }
                    DebouncedEvent::Remove(p) => {
                        green!("[Info] ");
                        cyan!("REMOVE -> ");
                        println!("{:?}", p);
                    }
                    DebouncedEvent::Rename(p, p2) => {
                        green!("[Info] ");
                        cyan!("RENAME -> ");
                        print!("{:?}", p);
                        cyan!(" to ");
                        println!("{:?}", p2);
                    }
                    DebouncedEvent::Rescan => {
                        dark_yellow!("[Warn] ");
                        println!("RESCAN");
                    }
                    DebouncedEvent::NoticeRemove(p) => {
                        green!("[Info] ");
                        cyan!("REMOVE NOTICE -> ");
                        println!("{:?}", p);
                    }
                    DebouncedEvent::NoticeWrite(p) => {
                        green!("[Info] ");
                        cyan!("WRITE NOTICE -> ");
                        println!("{:?}", p);
                    }
                    DebouncedEvent::Error(e, p) => {
                        if let Some(path) = p {
                            red!("[Error] ");
                            dark_yellow!("Path ");
                            print!("{:?}", path);
                            dark_yellow!(": ");
                            println!("{:?}", e);
                        } else {
                            red!("[Error] ");
                            dark_yellow!("Path Unknown: ");
                            println!("{:?}", e);
                        }
                    }
                    _ => {}
                }
            }
            let x = format!("{:?}", x);
            txw_tokio2.send(x).unwrap();
        }
    });

    let select_handle = tokio::spawn(async move {
        let mut count = 0;
        let mut shutdown_handler = ShutdownHandler::new().unwrap();
        loop {
            crossbeam::select! {
                recv(rxw_tokio) -> msg => {
                    println!("Got {:?} from watcher", msg);
                },
                default => {},
            }
            tokio::select! {
                shutdown = shutdown_handler.wait() => {
                    if let Ok(SigType::Interrupt) = shutdown {
                        println!("\nshutting down");
                        std::process::exit(1);
                    }
                },
            }
        }
    });

    stdin_handle.await.unwrap();
    watch_handle.await.unwrap();
    other_handle.await.unwrap();
    select_handle.await.unwrap();
}

async fn get_input(prompt: &str) -> Vec<String> {
    let mut lines = Vec::new();
    println!("Enter a directory path to watch:");
    let _ = tokio::io::stdout().flush();
    let stdin = tokio::io::stdin();
    let mut reader = BufReader::new(stdin);
    let mut buf = String::new();
    while let Ok(n) = reader.read_line(&mut buf).await {
        match n {
            0 => break,
            _ => {
                lines.push(buf.clone().replace("\n", ""));
                if buf.ends_with("\n") {
                    break;
                }
            }
        }
    }
    lines
}

fn format_path(s: PathBuf) -> PathBuf {
    let s = s
        .display()
        .to_string()
        .replace("\r", "")
        .replace("\n", "")
        .replace("\t", "");
    s.into()
}

#[allow(unused)]
enum SigType {
    Interrupt,
    Terminate,
}
#[cfg(unix)]
struct ShutdownHandler {
    sigint: Signal,
    sigterm: Signal,
}
#[cfg(unix)]
impl ShutdownHandler {
    fn new() -> Result<Self, std::io::Error> {
        let sigint = signal(SignalKind::interrupt())?;
        let sigterm = signal(SignalKind::terminate())?;

        Ok(ShutdownHandler { sigint, sigterm })
    }

    async fn wait(&mut self) -> Result<SigType, std::io::Error> {
        let sigtype;

        tokio::select! {
            _ = self.sigint.recv() => {
                sigtype = SigType::Interrupt
            },
            _ = self.sigterm.recv() => {
                sigtype = SigType::Terminate
            }
        }

        Ok(sigtype)
    }
}

#[cfg(windows)]
struct ShutdownHandler;

#[cfg(windows)]
impl ShutdownHandler {
    fn new() -> Result<Self, std::io::Error> {
        Ok(ShutdownHandler)
    }

    async fn wait(&mut self) -> Result<SigType, std::io::Error> {
        tokio::signal::ctrl_c().await?;

        Ok(SigType::Interrupt)
    }
}