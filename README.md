# Code Samples
Chris Shenkan <chris@shenkan.dev>

Code samples contained in this repository consist of:

- sdev-api: sample application with django + celery 
- file-watcher & excel_collector: two parts of what later became a file processing pipeline to ingest files, process them, and catalog them using postgres and influxdb databases
- pdfgrader: a utility to grade pdf multiple choice tests created *exactly* as requested as a favor for a friend (I would've preferred something with more capabilities)

note: projects are mostly in a raw form from the prototyping stage and cherry picked as examples in order to omit any code that is under license
