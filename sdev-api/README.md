# Django & Celery Example App
By Chris Shenkan  
chris@shenkan.dev

Note: This application is not production ready.  Many aspects are missng such as validation.  However it is functional.

Deploy with:
docker-compose up -d --build

Test with:
curl -sS http://localhost/transcoder/upload -F 'file=@/path/to/audio/file'
