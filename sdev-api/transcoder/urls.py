from django.urls import path
from . import views

urlpatterns = [
    path('upload', views.AudioFileUploadView.as_view(), name='transcoder-test'),
    path('', views.test, name='transcoder-home'),
]
