import uuid
from django.core.files.storage import FileSystemStorage
from django.db import models
from django.db.models.signals import post_delete
from django.dispatch import receiver
import os
from django.conf import settings

def unique_filename(instance, filename):
    new_filename = uuid.uuid4()
    return str(new_filename)

class AudioFile(models.Model):
    file = models.FileField(storage=FileSystemStorage(location=settings.MEDIA_ROOT), upload_to=unique_filename)
    output_file = models.FilePathField(blank=True, null=True, path=settings.MEDIA_ROOT)
    processed = models.BooleanField(default=False)

    def __str__(self):
        return self.file.name

@receiver(post_delete, sender=AudioFile)
def remove_files(sender, instance, **kwargs):
    if instance.file:
        if os.path.isfile(instance.file.path):
            os.remove(instance.file.path)
    if instance.processed:
        if os.path.isfile(instance.output_file):
            os.remove(instance.output_file)

