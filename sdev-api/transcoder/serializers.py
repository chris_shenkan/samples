from rest_framework import serializers
from .models import AudioFile

class AudioFileSerializer(serializers.ModelSerializer):
    file = serializers.FileField(use_url=True)
    class Meta:
        model = AudioFile
        fields = ("id", "file")
