import os
from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'sdev.settings.dev')

app = Celery('audio_transcoder')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()
