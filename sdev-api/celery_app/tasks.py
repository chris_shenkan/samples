import os
import subprocess

from celery_app.celery import app
from transcoder.models import AudioFile
from celery.utils.log import get_task_logger
from celery.signals import task_postrun

from django.conf import settings

logger = get_task_logger(__name__)


@app.task
def transcode(audio_id):
    audio_file = AudioFile.objects.get(id=audio_id)
    audio_file_path = audio_file.file.path
    filename = os.path.basename(audio_file_path)

    output_dir = os.path.join(settings.MEDIA_ROOT, "output")
    output_file_path = os.path.join(output_dir, f"{filename}.flac")

    if not os.path.isdir(os.path.dirname(output_file_path)):
        logger.debug("creating directory %s", os.path.dirname(output_file_path))
        os.makedirs(os.path.dirname(output_file_path))

    logger.info("transcoding...")
    subprocess.call(["/usr/bin/ffmpeg", "-i", audio_file_path, output_file_path])
    logger.info("transcoding complete")

    audio_file.output_file = output_file_path
    audio_file.save()


@task_postrun.connect(sender=transcode)
def task_postrun_handler(sender=None, body=None, **kwargs):
    audio_file = AudioFile.objects.get(id=kwargs["args"][0])
    audio_file.processed = True
    audio_file.save()
    logger.debug("processed audio file")
